from flask import Flask, jsonify , render_template, request
app = Flask(__name__)

students=[{'id':"1",
           'name':"Simran",
           'email':"sim@gmail.com",
           'contact_number':"787878",
           'address':"Indore"
           },
          {'id': "2",
           'name': "Shalu",
           'email': "shalu@gmail.com",
           'contact_number': "787878",
           'address': "Indore"

           }
          ]

@app.route('/')
def index():
   n=len(students)
   return "hello student"


@app.route("/students",methods=['GET'])
def get():
   return jsonify({'Students':students})

@app.route('/students/<int:id>',methods=['GET'])
def get_Student(id):
    return  jsonify({'student':students[id]})

@app.route('/add')
def add_Student():
    return render_template('StudentForm.html')
@app.route('/', methods=['POST'])

def getValue():
   name=request.form['name']
   email=request.form['email']
   contact_number=request.form['contact_number']
   address=request.form['address']
   student={
           'id':len(students)+1,
          'name':name,
           'email':email,
            'contact_number':contact_number,
            'address':address
   }
   students.append(student)
   return "add Successfully"


if __name__ == '__main__':
   app.run(debug = True)


